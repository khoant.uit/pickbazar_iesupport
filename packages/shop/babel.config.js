module.exports = function (api) {
  api.cache(true);

  const presets = [
    [
      'next/babel',
      {
        'preset-env': {
          targets: {
            ie: '11',
          },
        },
      },
    ],
  ];
  const plugins = [
    [
      'styled-components',
      {
        ssr: true,
      },
    ],
  ];

  return {
    presets,
    plugins,
  };
};
